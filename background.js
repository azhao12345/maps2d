chrome.webRequest.onBeforeRequest.addListener(
    (details) => {
        // check that it's google and we're not already 2d
        var url = new URL(details.url);
        if (!url.host.startsWith('www.google')
            || details.url.endsWith('force=canvas')) {
            return {};
        }
        // generate the url
        url.searchParams.set('force', 'canvas');
        return {
            redirectUrl: url.href
        }
    },
    {
        urls: [
          "https://*/maps*",
        ],
        types: ["main_frame"]
    },
    ["blocking"]
)
